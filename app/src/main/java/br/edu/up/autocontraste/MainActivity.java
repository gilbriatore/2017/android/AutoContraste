package br.edu.up.autocontraste;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
    implements SensorEventListener {

  private Sensor sensor;
  private SensorManager sm;
  private LinearLayout layout;
  private TextView textView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    layout = (LinearLayout) findViewById(R.id.activity_main);
    textView = (TextView) findViewById(R.id.txtMensagem);
    sm = (SensorManager) getSystemService(SENSOR_SERVICE);
    sensor = sm.getDefaultSensor(Sensor.TYPE_LIGHT);
  }

  @Override
  protected void onPause() {
    super.onPause();
    sm.unregisterListener(this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    sm.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    sm = null;
  }

  @Override
  public void onSensorChanged(SensorEvent event) {

    if (event.values.length > 0) {

      int brilho = 255; //Escuro;
      float luz = event.values[0];

      if(luz < 25) { //Pouco escuro;
        brilho = 200;
      } else if(luz < 50) { //Iluminado;
        brilho = 150;
      }else if(luz < 100) { //Bem iluminado;
        brilho = 100;
      }

      if (brilho < 151){
        layout.setBackgroundColor(Color.BLACK);
        textView.setTextColor(Color.parseColor("#FFFFBB33"));
      } else {
        layout.setBackgroundColor(Color.WHITE);
        textView.setTextColor(Color.BLACK);
      }

      Settings.System.putInt(getContentResolver(),
          Settings.System.SCREEN_BRIGHTNESS, brilho);
      Window window = getWindow();
      WindowManager.LayoutParams p = window.getAttributes();
      p.screenBrightness = brilho;
      window.setAttributes(p);
    }
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
    //Não implementado.
  }
}
